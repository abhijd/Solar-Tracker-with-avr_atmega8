# Solar Tracker with avr atmega8

A simple and inexpensive single-axis Solar Tracker implementation with AVR Atmega 8 microcontroller and peripherals 

## Simulating

To simulate, you must install all the softwares listed in the prerequisite section and then following the "How to simulate" section 

### Prerequisites

 * Proteus Design Suite


### How-to-simulate
 
 * Clone the repo in your local directory
 * Start Proteus Design Suite
 * In Proteus, open either "solar_tracker_avr_L298" or "solar_tracker_avr_simplified"  
 * When the design opens, double-click on the Atmega8 microcontroller that opens the "edit component" pop-up
 * From the edit component, add the location of the main.hex file (located in the folder solar_tracker_with avr) in the program file field and click OK.
 * Press the play button from the bottom left menu. Voila!!
 * Once started, you can change the light intensity of the two light source to imitate the movement of the sun
 
## Changing the program code

  * To change the program code, you may use AVR Studio/WinAVR to open the main.c file (located in the folder solar_tracker_with avr) and change.
  * Once changed, compile the hex file again using AVR Studio/ WinAVR, and load it again to the Atmega8 following the steps from "How-to-simulate"
  * If you are using WinAVR you may use the Makefile in the folder solar_tracker_with avr to quickly configure the compiler. 
  
## Hardware Implementation 

  * You must select the model of motor and its controller according to the frame and weight of installation. 
  * A simple voltage regulator circuit should be used to power the microcontroller
  * Before printing the circuit in board, experimentally make sure the hardware implementation work because the real world is not always similar to simulation world. We had to do several tweaks to make it work in the real world.  
  * The main.hex file must me burned (not literally) in the Atmega8 microcontroller using AVR programmer or some universal programmer that supports it.
  

 
## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required to do the project. 

